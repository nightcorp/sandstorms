Adds sandstorms to desert and arid tiles. Functionally very similar to snow and rain, but breaks outdoors electronics rather than short-circuiting them.

Sandstorms will produce extremely windy weather and severely affect sight and movement of pawns on the map.

During sandstorms sand will slowly gather on the map, and sand being sand, it gets everywhere, even under roofed areas. Fallen sand will slowly be blown away by the wind on the map.

Pawns will also clear fallen sand in any designated snow clearing zone, which now also functions as a sand clearing zone.