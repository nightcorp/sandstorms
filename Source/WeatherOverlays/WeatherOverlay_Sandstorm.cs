﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Sandstorms
{
    /// <remarks>
    /// Making my own Material with shaders and all is over my skills, instead we are imitating the approach used by https://github.com/AaronCRobinson/SeasonalWeather, which combines fog and hard snow materials
    /// </remarks>

    [StaticConstructorOnStartup]
    public class WeatherOverlay_SandstormCloud : SkyOverlay
    {
        private static readonly Material CloudMaterial = (Material)AccessTools.Field(typeof(WeatherOverlay_Fog), "FogOverlayWorld").GetValue(null);

        public WeatherOverlay_SandstormCloud()
        {
            worldOverlayMat = CloudMaterial;
            worldOverlayPanSpeed1 = 0.03f;
            worldPanDir1 = new Vector2(1f, -0.25f);
            worldPanDir1.Normalize();
            worldOverlayPanSpeed2 = 0.02f;
            worldPanDir2 = new Vector2(1f, -0.24f);
            worldPanDir2.Normalize();
        }
    }

    [StaticConstructorOnStartup]
    public class WeatherOverlay_SandstormDust : SkyOverlay
    {
        private static readonly Material DustMaterial = (Material)AccessTools.Field(typeof(WeatherOverlay_SnowHard), "SnowOverlayWorld").GetValue(null);

        public WeatherOverlay_SandstormDust()
        {
            worldOverlayMat = DustMaterial;
            worldOverlayPanSpeed1 = 0.09f;
            worldPanDir1 = new Vector2(1f, -0.25f);
            worldPanDir1.Normalize();
            worldOverlayPanSpeed2 = 0.06f;
            worldPanDir2 = new Vector2(1f, -0.24f);
            worldPanDir2.Normalize();
        }
    }
}
