﻿#if v1_5
using LudeonTK;
#endif
using RimWorld;
using RimWorld.Planet;
using System;
using Sandstorms.Settings;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Sandstorms
{
    public static class WorldUtility
    {
        static List<int> workingList = new List<int>();
        static bool hasFoundAnyDesert = false;
        static bool shouldStopAtFirstFind = false;

        public static List<int> FindNearbyDeserts(int rootTile, bool stopAtFirstFind = true)
        {
            workingList.Clear();
            hasFoundAnyDesert = false;
            shouldStopAtFirstFind = stopAtFirstFind;
            int maxTilesToProcess = Find.World.grid.TilesNumWithinTraversalDistance(NCSS_Mod.Settings.DistanceToSearchForSandstormBiome);
            Find.WorldFloodFiller.FloodFill(rootTile, (int t) => true, TileProcessor, maxTilesToProcess);

            return workingList;
        }

        private static bool TileValidator(int tile)
        {
            BiomeDef biome = Find.World.grid[tile].biome;
            return WeatherUtility.CanHaveSandstorms(biome);
        }

        private static bool TileProcessor(int tile, int distanceTraversed)
        {
            if (shouldStopAtFirstFind && hasFoundAnyDesert)
            {
                return true;
            }
            if(TileValidator(tile))
            {
                hasFoundAnyDesert = true;
                workingList.Add(tile);
            }
            return false;
        }

        [DebugAction("Sandstorms", "Get nearby deserts", actionType = DebugActionType.ToolWorld, allowedGameStates = AllowedGameStates.PlayingOnWorld)]
        public static void DebugGetNearbyDeserts()
        {
            int currentTile = GenWorld.MouseTile();
            List<int> deserts = FindNearbyDeserts(currentTile, false);
            Find.World.debugDrawer.FlashTile(currentTile, 0.5f);
            Log.Message($"Deserts near {currentTile}: {string.Join(", ", deserts)}");
            foreach (int desertTile in deserts)
            {
                float colorPct = desertTile == currentTile ? 0.5f : 0f;
                Find.World.debugDrawer.FlashTile(desertTile, colorPct);
            }
        }

        [DebugAction("Sandstorms", "Get first nearby desert", actionType = DebugActionType.ToolWorld, allowedGameStates = AllowedGameStates.PlayingOnWorld)]
        public static void DebugGetFirstNearbyDesert()
        {
            int currentTile = GenWorld.MouseTile();
            List<int> deserts = FindNearbyDeserts(currentTile);
            Find.World.debugDrawer.FlashTile(currentTile, 0.5f);
            Log.Message($"Deserts near {currentTile}: {string.Join(", ", deserts)}");
            foreach (int desertTile in deserts)
            {
                float colorPct = desertTile == currentTile ? 0.5f : 0f;
                Find.World.debugDrawer.FlashTile(desertTile, colorPct);
            }
        }
    }
}
