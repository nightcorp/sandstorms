﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse.AI;
using Verse;

namespace Sandstorms
{
    public static class NCSS_JobUtility
    {
        public static void ClearSandAtTargetA(JobDriver jobDriver)
        {
            jobDriver.pawn.Map.GetComponent<SandGrid>().RemoveSand(jobDriver.job.targetA.Cell);
        }
    }
}
