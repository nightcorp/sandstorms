﻿#if v1_5
using LudeonTK;
#endif
using HarmonyLib;
using RimWorld;
using Sandstorms.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Sandstorms
{
    public static class SandUtility
    {
        const float maxMountainnessForSand = 0.5f;
        public static bool SandCanBeOn(IntVec3 cell, Map map, out bool shouldRemoveSandImmediately)
        {
            shouldRemoveSandImmediately = false;
            Room room = cell.GetRoom(map);
            bool isOutdoors = room == null || room.UsesOutdoorTemperature;
            if (!isOutdoors)
            {
                return false;
            }

            // if it's roofed, check if we are under a mountain
            RoofDef roof = map.roofGrid.RoofAt(cell);
            if (roof != null && roof == RoofDefOf.RoofRockThick)
            {
                return false;
            }

            TerrainDef terrainDef = map.terrainGrid.TerrainAt(cell);
            if (terrainDef.IsWater)
            {
                // the mod "Water Freezes" makes water turn into ice. Ice can take sand, but water cannot, so water cells must be permitted to remove sand if the sand was added on ice
                shouldRemoveSandImmediately = true;
            }
            // no terrain or terrain holds snow
            bool canHoldSand = terrainDef == null || terrainDef.holdSnow;
            // sand-terrain can not have sand on top of it (I guess it *can*, but clearing sand on top of sand?)
            // now we allow the user to decide if they want this
            if (!NCSS_Mod.Settings.CanFallenSandBeOnSandTerrain)
            {
                canHoldSand &= terrainDef != TerrainDefOf.Sand && terrainDef != NCSS_TerrainDefOf.SoftSand;
            }
            return canHoldSand;
        }

        static Func<IntVec3, Map, float> detouredGetMountainnessScoreAt;
        private static float DETOUR_GetMountainousnessScoreAt(IntVec3 cell, Map map)
        {
            if(detouredGetMountainnessScoreAt == default)
            {
                MethodInfo getMountainnessScoreAtMI = AccessTools.Method(typeof(InfestationCellFinder), "GetMountainousnessScoreAt");
                detouredGetMountainnessScoreAt = Delegate.CreateDelegate(typeof(Func<IntVec3, Map, float>), getMountainnessScoreAtMI) as Func<IntVec3, Map, float>;
            }

            return detouredGetMountainnessScoreAt(cell, map);
        }

        public static float CurrentSandRate(Map map)
        {
            if(map == null)
            {
                string message = $"Tried to retrieve sand rate for NULL map";
                Log.WarningOnce(message, message.GetHashCode());
                return 0;
            }
            WeatherDef lastWeather = map.weatherManager?.lastWeather;
            WeatherDef currentWeather = map.weatherManager.curWeather;
            float currentWeatherSandRate = currentWeather == NCSS_WeatherDefOf.NCSS_Sandstorm ? 1f : 0f;
            if(lastWeather == null)
            {
                return currentWeatherSandRate;
            }

            float previousWeatherSandRate = lastWeather == NCSS_WeatherDefOf.NCSS_Sandstorm ? 1f : 0f;
            return Mathf.Lerp(previousWeatherSandRate, currentWeatherSandRate, map.weatherManager.TransitionLerpFactor);
        }

        [DebugAction("Sandstorms", "Sand flood map", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        private static void DebugSandFloodMap()
        {
            Map map = Find.CurrentMap;
            SandGrid sandGrid = map.GetComponent<SandGrid>();
            foreach(IntVec3 cell in map.AllCells)
            {
                if(SandCanBeOn(cell, map, out _))
                {
                    sandGrid.AddDepth(cell, 1);
                }
            }
        }
        [DebugAction("Sandstorms", "Sand flood map (force all tiles)", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        private static void DebugSandFloodMapForceAllTiles()
        {
            Map map = Find.CurrentMap;
            SandGrid sandGrid = map.GetComponent<SandGrid>();
            foreach(IntVec3 cell in map.AllCells)
            {
                sandGrid.AddDepth(cell, 1);
            }
        }
    }
}
