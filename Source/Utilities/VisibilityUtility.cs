﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Sandstorms
{
    public static class VisibilityUtility
    {
        public static bool CanBeSeenUnderSand(this Thing thing)
        {
            SandGrid sandGrid = thing.Map?.GetComponent<SandGrid>();
            if(sandGrid == null)
            {
                return true;
            }
            float currentDepth = sandGrid.GetDepth(thing.Position);
            return thing.def.hideAtSnowDepth >= currentDepth;
        }
        public static float SandOrSnowDepth(Thing thing)
        {
            float snowDepth = thing.Map.snowGrid.GetDepth(thing.Position);
            float sandDepth = thing.Map.GetComponent<SandGrid>().GetDepth(thing.Position);
            return Mathf.Max(snowDepth, sandDepth);
        }
    }
}
