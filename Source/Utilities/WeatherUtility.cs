﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Sandstorms
{
    public static class WeatherUtility
    {
        public static bool CanHaveSandstorms(BiomeDef biomeDef)
        {
            return biomeDef.baseWeatherCommonalities.Any(wc => wc.weather == NCSS_WeatherDefOf.NCSS_Sandstorm && wc.commonality >= 1);
        }
    }
}
