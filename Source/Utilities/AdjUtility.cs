﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using static UnityEngine.UI.Image;

namespace Sandstorms
{
    public static class AdjUtility
    {
        static AdjUtility()
        {
            Cells8WayAndInsideForShaderOffsets = new IntVec3[9];

            for (int i = 1; i < GenAdj.AdjacentCellsAndInsideForUV.Length - 1; i++)
            {
                IntVec3 offset = GenAdj.AdjacentCellsAndInsideForUV[i];
                Cells8WayAndInsideForShaderOffsets[i-1] = offset;
            }
            Cells8WayAndInsideForShaderOffsets[7] = GenAdj.AdjacentCellsAndInsideForUV[0];
            Cells8WayAndInsideForShaderOffsets[8] = GenAdj.AdjacentCellsAndInsideForUV[8];
        }

        public static IntVec3[] Cells8WayAndInsideForShaderOffsets;

        public static IEnumerable<IntVec3> Cells8WayAndInside(IntVec3 origin)
        {
            for (int i = 1; i < GenAdj.AdjacentCellsAndInsideForUV.Length - 1; i++)
            {
                IntVec3 offset = GenAdj.AdjacentCellsAndInsideForUV[i];
                yield return origin + offset;
            }
            yield return origin + GenAdj.AdjacentCellsAndInsideForUV[0];
            yield return origin + GenAdj.AdjacentCellsAndInsideForUV[8];
        }
    }
}
