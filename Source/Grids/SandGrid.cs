﻿#if v1_5
using LudeonTK;
#endif
using HarmonyLib;
using RimWorld;
using RimWorld.Planet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Sandstorms
{
    public class SandGrid : MapComponent
    {
        //public ByteGrid ByteGrid => byteGrid;

        //ByteGrid byteGrid;
        float[] depthGrid;
        public bool AnySandAvailable = false;

        public SandGrid(Map map) : base(map)
        {
            //byteGrid = new ByteGrid(map);
            depthGrid = new float[map.cellIndices.NumGridCells];
        }

        public void AddDepth(IntVec3 cell, float amount)
        {
            int cellIndex = base.map.cellIndices.CellToIndex(cell);
            float oldDepth = depthGrid[cellIndex];
            if(oldDepth <= 0 && amount < 0f)
            {
                return;
            }
            if (!this.CanHaveSand(cellIndex))
            {
                this.depthGrid[cellIndex] = 0f;
                return;
            }
            if (oldDepth >= 0.999f && amount > 1f)
            {
                return;
            }
            float newDepth = oldDepth + amount;
            newDepth = Mathf.Clamp(newDepth, 0f, 1f);

            depthGrid[cellIndex] = newDepth;
            CheckMapSectionDirty(cell, oldDepth, newDepth);
        }

        public override void MapComponentTick()
        {
            base.MapComponentTick();
            Patch_SteadyEnvironmentEffects.RecacheSandValues(map);
            if(GenTicks.TicksGame % GenTicks.TickRareInterval == 0)
            {
                AnySandAvailable = depthGrid.Any(depth => depth > 0.01f);
            }
        }

        public void RemoveSand(IntVec3 cell)
        {
            if (!cell.InBounds(map))
            {
                return;
            }
            int cellIndex = base.map.cellIndices.CellToIndex(cell);
            float oldDepth = depthGrid[cellIndex];
            depthGrid[cellIndex] = 0f;
            CheckMapSectionDirty(cell, oldDepth, 0f);
        }

        public float GetDepth(IntVec3 cell, float originalCellDepth = 0)
        {
            if (!cell.InBounds(map))
            {
                return originalCellDepth;
            }
            int cellIndex = base.map.cellIndices.CellToIndex(cell);
            return depthGrid[cellIndex];
        }

        public SnowCategory GetDepthAsCategory(IntVec3 cell)
        {
            float depthAsFraction = GetDepth(cell);
            return SnowUtility.GetSnowCategory(depthAsFraction);
        }

        public bool CanHaveSand(int cellIndex)
        {
            Building building = base.map.edificeGrid[cellIndex];
            if (building != null && !CanCoexistWithSand(building.def))
            {
                return false;
            }
            return true;
        }

        public static bool CanCoexistWithSand(ThingDef def)
        {
            return SnowGrid.CanCoexistWithSnow(def);
        }

        private static ushort SandFloatToShort(float depth)
        {
            depth = Mathf.Clamp(depth, 0f, 1f);
            depth *= 65535f;
            return (ushort)Mathf.RoundToInt(depth);
        }
        private static float SandShortToFloat(ushort depth)
        {
            return (float)depth / 65535f;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            MapExposeUtility.ExposeUshort(this.map, (IntVec3 c) => SandFloatToShort(this.GetDepth(c)), delegate (IntVec3 c, ushort val)
            {
                this.depthGrid[this.map.cellIndices.CellToIndex(c)] = SandShortToFloat(val);
            }, "depthGrid");
        }

        [DebugAction("Sandstorms", "Clear Sand", actionType = DebugActionType.ToolMap, allowedGameStates = AllowedGameStates.IsCurrentlyOnMap)]
        public static void DebugClearSand()
        {
            SandGrid sandGrid = Find.CurrentMap.GetComponent<SandGrid>();
            for (int i = 0; i < sandGrid.depthGrid.Length; i++)
            {
                sandGrid.depthGrid[i] = 0;
            }
#if v1_5
            Find.CurrentMap.mapDrawer.WholeMapChanged(MapMeshFlagDefOf.Snow.index);
            Find.CurrentMap.mapDrawer.WholeMapChanged(MapMeshFlagDefOf.Things.index);
#else
            Find.CurrentMap.mapDrawer.WholeMapChanged(MapMeshFlag.Snow);
            Find.CurrentMap.mapDrawer.WholeMapChanged(MapMeshFlag.Things);
#endif
        }

        #region Map recache

        Dictionary<Section, SectionCacheValues> sectionCache = new Dictionary<Section, SectionCacheValues>();
        const int maxTicksUntilForcedDirty = 4 * GenTicks.TickRareInterval;
        [TweakValue("Sandstorms", 0.1f, 50f)]
        static float maxSectionDepthDeltaUntilForcedDirty = 12f;
        private void CheckMapSectionDirty(IntVec3 cell, float oldDepth, float newDepth)
        {
            if (Mathf.Approximately(oldDepth, newDepth))
            {
                return;
            }
            Section section = map.mapDrawer.SectionAt(cell);
            if (!sectionCache.ContainsKey(section))
            {
                sectionCache.Add(section, new SectionCacheValues(sectionCache.Count));
                sectionCache[section].Reset();
            }
            SectionCacheValues cacheEntry = sectionCache[section];
            // don't want all sections to update at the exact same tick, spread out the updates based on index of section
            // do not worry about Count being 0 here, the previous code forces at least one entry
            int tickOffset = (int)((float)cacheEntry.Index / sectionCache.Count * maxTicksUntilForcedDirty);
            int ticksSinceLastDirty = GenTicks.TicksGame - cacheEntry.LastUpdatedTick - tickOffset;
            if(ticksSinceLastDirty > maxTicksUntilForcedDirty)
            {
                cacheEntry.Reset();
                SetMapDirty(cell);
            }
            else
            {
                cacheEntry.TotalDepthDelta += Mathf.Abs(oldDepth - newDepth);
                if(cacheEntry.TotalDepthDelta > maxSectionDepthDeltaUntilForcedDirty)
                {
                    cacheEntry.Reset();
                    SetMapDirty(cell);
                }
                // pawns clearing sand will force 0 depth, this should be reflected visually
                else if(newDepth == 0)
                {
                    cacheEntry.Reset();
                    SetMapDirty(cell);
                }
            }

            //if (Mathf.Abs(oldDepth - newDepth) > differenceToConsiderDirty)
            //{
            //    this.map.mapDrawer.MapMeshDirty(cell, MapMeshFlag.Snow, true, false);
            //    this.map.mapDrawer.MapMeshDirty(cell, MapMeshFlag.Things, true, false);
            //}
            //else if (newDepth == 0f)
            //{
            //    this.map.mapDrawer.MapMeshDirty(cell, MapMeshFlag.Snow, true, false);
            //}
            if (SnowUtility.GetSnowCategory(oldDepth) != SnowUtility.GetSnowCategory(newDepth))
            {
                if (Rand.Chance(0.5f))
                {
                    this.map.pathing.RecalculatePerceivedPathCostAt(cell);
                }
            }
        }

        private void SetMapDirty(IntVec3 cell)
        {
#if v1_5
            Find.CurrentMap.mapDrawer.MapMeshDirty(cell, MapMeshFlagDefOf.Snow.index, true, false);
            Find.CurrentMap.mapDrawer.MapMeshDirty(cell, MapMeshFlagDefOf.Things.index, true, false);
#else
            this.map.mapDrawer.MapMeshDirty(cell, MapMeshFlag.Snow, true, false);
            this.map.mapDrawer.MapMeshDirty(cell, MapMeshFlag.Things, true, false);
#endif
        }
#endregion
    }

}
