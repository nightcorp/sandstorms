﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Sandstorms
{
    public class SectionCacheValues
    {
        public int LastUpdatedTick;
        public float TotalDepthDelta;
        public int Index;

        public SectionCacheValues(int index)
        {
            Index = index;
            Reset();
        }

        public SectionCacheValues(int lastUpdatedTick, float totalDepthDelta)
        {
            this.LastUpdatedTick = lastUpdatedTick;
            this.TotalDepthDelta = totalDepthDelta;
        }

        public void Reset()
        {
            LastUpdatedTick = GenTicks.TicksGame;
            TotalDepthDelta = 0f;
        }
    }
}
