﻿#if v1_5
using LudeonTK;
#endif
using RimWorld;
using Sandstorms.Settings;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UIElements;
using Verse;

namespace Sandstorms
{
    [StaticConstructorOnStartup]
    public class SectionLayer_Sand : SectionLayer
    {
        private static readonly Texture2D sandTexture;
        private static readonly Material sandMaterial;

        static SectionLayer_Sand()
        {
            //sandTexture = ContentFinder<Texture2D>.Get("Sandstorms/sand");
            string sandTexturePath = NCSS_Mod.Settings.UseAlternativeSandTexture ? "Sandstorms/sand" : "Terrain/Surfaces/Sand";
            sandTexture = ContentFinder<Texture2D>.Get(sandTexturePath);
            sandMaterial = new Material(MatBases.Snow);
            sandMaterial.SetTexture("_MainTex", sandTexture);
            sandMaterial.SetTexture("_MacroTex", sandTexture);
            sandMaterial.SetTexture("_AlphaAddTex ", sandTexture);
            sandMaterial.SetTexture("_PollutedTex", sandTexture);
        }

        public static readonly List<List<int>> vertexWeights = new List<List<int>>
        {
            new List<int> { 0, 1, 7, 8 },
            new List<int> { 1, 8 },
            new List<int> { 1, 2, 3, 8 },
            new List<int> { 3, 8 },
            new List<int> { 3, 4, 5, 8 },
            new List<int> { 5, 8 },
            new List<int> { 5, 6, 7, 8 },
            new List<int> { 7, 8 },
            new List<int> { 8 }
        };

        public SectionLayer_Sand(Section section) : base(section)
        {
#if v1_5
            base.relevantChangeTypes = MapMeshFlagDefOf.Snow.index;
#else
            base.relevantChangeTypes = MapMeshFlag.Snow;
#endif
        }

        public override void Regenerate()
        {
            LayerSubMesh subMesh = base.GetSubMesh(sandMaterial);
            SandGrid sandGrid = base.Map.GetComponent<SandGrid>();

            if (subMesh.mesh.vertexCount == 0)
            {
                SectionLayerGeometryMaker_Solid.MakeBaseGeometry(this.section, subMesh, AltitudeLayer.Terrain);
            }
            subMesh.Clear(MeshParts.Colors);
            CellRect cellRect = base.section.CellRect;

            bool canDrawAnything = true;
            for (int x = cellRect.minX; x <= cellRect.maxX; x++)
            {
                for (int z = cellRect.minZ; z <= cellRect.maxZ; z++)
                {
                    IntVec3 cell = new IntVec3(x, 0, z);
                    float[] cellDepths = CalculateDepths(cell, sandGrid);
                    canDrawAnything |= cellDepths.Any(d => d > 0.01f);
                    IEnumerable<float> opacities = CalculateOpacities(cellDepths);
                    foreach (float opacity in opacities)
                    {
                        subMesh.colors.Add(new Color32(255, 255, 0, Convert.ToByte(opacity * 255)));
                    }
                }
            }
            subMesh.disabled = !canDrawAnything;
            if (canDrawAnything)
            {
                subMesh.FinalizeMesh(MeshParts.Colors);
            }
        }

        private static float[] CalculateDepths(IntVec3 origin, SandGrid sandGrid)
        {
            float originDepth = sandGrid.GetDepth(origin);
            float[] depths = new float[9];
            for (int i = 0; i < AdjUtility.Cells8WayAndInsideForShaderOffsets.Length; i++)
            {
                IntVec3 offset = AdjUtility.Cells8WayAndInsideForShaderOffsets[i];
                depths[i] = sandGrid.GetDepth(origin + offset, originDepth);
            }
            return depths;
            /* LINQ is not performant here
            return AdjUtility.Cells8WayAndInside(origin)
                .Select(c => sandGrid.GetDepth(c, originDepth))
                .ToArray();
            */
        }

        private static IEnumerable<float> CalculateOpacities(float[] depths)
        {
            /* Performance matters drastically at this low level, do not use LINQ :(
            for (int i = 0; i < 9; i++)
            {
                float opacity = vertexWeights[i].Average(offset => depths[offset]);
                yield return opacity;
            }
            */
            for (int i = 0; i < 9; i++)
            {
                int neighborCount = vertexWeights[i].Count;
                float opacity = 0f;
                for (int j = 0; j < neighborCount; j++)
                {
                    int neighborIndex = vertexWeights[i][j];
                    opacity += depths[neighborIndex];
                }
                opacity /= neighborCount;
                yield return opacity;
            }
        }

        [DebugAction("Sandstorms", "Sand opacity check", actionType = DebugActionType.ToolMap, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        private static void DebugOpacityCheck()
        {
            IntVec3 cell = UI.MouseCell();
            Map map = Find.CurrentMap;
            IntVec3[] cells = AdjUtility.Cells8WayAndInside(cell).ToArray();
            float[] depths = CalculateDepths(cell, map.GetComponent<SandGrid>());
            float[] opacities = CalculateOpacities(depths).ToArray();
            for (int i = 0; i < cells.Length; i++)
            {
                map.debugDrawer.FlashCell(cells[i], text: $"{Math.Round(depths[i], 2)}\n{Math.Round(opacities[i], 2)}");
            }
        }
    }
}
