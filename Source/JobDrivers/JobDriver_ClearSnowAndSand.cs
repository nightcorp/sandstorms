﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace Sandstorms
{
    public class JobDriver_ClearSnowAndSand : JobDriver
    {
        private float workDone = 0;
        private bool shouldDisplaySnowEffect = false;
        private bool shouldDisplaySandEffect = false;

        SandGrid SandGrid => base.Map.GetComponent<SandGrid>();
        SnowGrid SnowGrid => base.Map.snowGrid;

        public float TotalNeededWork => SandGrid.GetDepth(TargetLocA) * clearWorkPerDepth
                + SnowGrid.GetDepth(TargetLocA) * clearWorkPerDepth;

        const TargetIndex cellIndex = TargetIndex.A;
        // snow grid uses 0-1 - sand grid uses 0-255, so we adapt the numbers accordingly
        const float clearWorkPerDepth = 50f;

        public override bool TryMakePreToilReservations(bool errorOnFailed)
        {
            shouldDisplaySandEffect = SandGrid.GetDepth(TargetLocA) > 0;
            shouldDisplaySnowEffect = SnowGrid.GetDepth(TargetLocA) > 0;

            return base.pawn.Reserve(job.GetTarget(cellIndex), base.job);
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            yield return Toils_Goto.GotoCell(TargetIndex.A, PathEndMode.Touch);
            Toil clearToil = ToilMaker.MakeToil("MakeNewToils");
            clearToil.AddPreTickAction(() =>
            {
                float clearValue = pawn.GetStatValue(StatDefOf.GeneralLaborSpeed);
                workDone += clearValue;
                if (workDone >= TotalNeededWork)
                {
                    SandGrid.RemoveSand(TargetLocA);
                    SnowGrid.SetDepth(TargetLocA, 0);
                    ReadyForNextToil();
                }
            });
            clearToil.defaultCompleteMode = ToilCompleteMode.Never;
            if(shouldDisplaySandEffect)
            {
                clearToil.WithEffect(NCSS_EffecterDefOf.NCSS_ClearSand, cellIndex, null);
            }
            if(shouldDisplaySnowEffect)
            {
                clearToil.WithEffect(EffecterDefOf.ClearSnow, cellIndex, null);
            }
            clearToil.PlaySustainerOrSound(() => SoundDefOf.Interact_CleanFilth);
            clearToil.WithProgressBar(cellIndex, () => workDone / TotalNeededWork, true);
            clearToil.FailOnCannotTouch(cellIndex, PathEndMode.Touch);

            yield return clearToil;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref shouldDisplaySandEffect, nameof(shouldDisplaySandEffect));
            Scribe_Values.Look(ref shouldDisplaySnowEffect, nameof(shouldDisplaySnowEffect));
        }
    }
}
