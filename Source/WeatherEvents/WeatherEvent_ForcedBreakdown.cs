﻿#if v1_5
using LudeonTK;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using RimWorld;
using Sandstorms.Settings;
using Verse;

namespace Sandstorms
{
    public class WeatherEvent_ForcedBreakdown : WeatherEvent
    {
        // remember the last comps so we don't need to run expensive reflection every time
        static Map lastAccessedMap;
        static List<CompBreakdownable> breakdownableComps;
        static readonly FieldInfo breakdownablesFieldInfo = AccessTools.Field(typeof(BreakdownManager), "comps");
        private bool _expired = false;
        public WeatherEvent_ForcedBreakdown(Map map) : base(map) { }

        public override bool Expired => _expired;

        public override void FireEvent()
        {
            _expired = true;
            if(!NCSS_Mod.Settings.EnableBreakdowns)
            {
                return;
            }
            if(!Rand.Chance(NCSS_Mod.Settings.ChanceToBreakDown))
            {
                return;
            }
            RecacheBreakdownableComps(map);
            if(breakdownableComps.NullOrEmpty())
            {
                return;
            }
            CompBreakdownable randomBreakdownableComp = breakdownableComps
                .Where(c => SandstormCanBreakDown(c))
                .RandomElementWithFallback();
            if(randomBreakdownableComp == null)
            {
                return;
            }

            randomBreakdownableComp.DoBreakdown();
            Thing brokenThing = randomBreakdownableComp.parent;
            Messages.Message("NCSS_SandstormBreakdown".Translate(brokenThing), brokenThing, MessageTypeDefOf.NegativeEvent);
        }

        private static void RecacheBreakdownableComps(Map map)
        {
            if(lastAccessedMap == map)
            {
                return;
            }
            lastAccessedMap = map;
            BreakdownManager breakdownManager = map.GetComponent<BreakdownManager>();
            breakdownableComps = (List<CompBreakdownable>)breakdownablesFieldInfo.GetValue(breakdownManager);
        }

        private static bool SandstormCanBreakDown(CompBreakdownable comp)
        {
            if(comp.BrokenDown)
            {
                return false;
            }
            ThingExtension_SandstormBreakdown breakdownExtension = comp.parent.def.GetModExtension<ThingExtension_SandstormBreakdown>();
            if(breakdownExtension != null)
            {
                if(breakdownExtension.immuneToBreakdown)
                {
                    return false;
                }
            }
            ThingWithComps thing = comp.parent;
            List<IntVec3> cellsToCheck = new List<IntVec3>();
            if (comp.parent.def?.PlaceWorkers?.Any(pw => pw is PlaceWorker_Cooler) == true)
            {
                // coolers and the likes are weird, because a building over a wall is always considered outdoors, use the adjacent cells for the calculation instead.
                IntVec3 northernCell = thing.Position + IntVec3.North.RotatedBy(thing.Rotation);
                IntVec3 southernCell = thing.Position + IntVec3.South.RotatedBy(thing.Rotation);
                cellsToCheck.Add(northernCell);
                cellsToCheck.Add(southernCell);
            }
            else if (thing.def.hasInteractionCell)
            {
                cellsToCheck.Add(thing.InteractionCell);
            }
            else
            {
                cellsToCheck.Add(thing.Position);
            }
            foreach (IntVec3 cell in cellsToCheck)
            {
                bool canBreak = SandUtility.SandCanBeOn(cell, thing.Map, out _);
                //Log.Message($"Can break on cell {cell} ? {canBreak}");
                if (canBreak)
                {
                    return true;
                }
            }
            return false;
        }

        public override void WeatherEventTick()
        {

        }

        [DebugAction("Sandstorms", "Can Sandstorm break this?", actionType = DebugActionType.ToolMap, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void Debug_CanSandstormsBreakThis()
        {
            List<Thing> things = UI.MouseCell().GetThingList(Find.CurrentMap);
            foreach (Thing thing in things)
            {
                CompBreakdownable breakdownComp = thing.TryGetComp<CompBreakdownable>();
                if(breakdownComp == null)
                {
                    Log.Message($"Cannot break {thing.Label}, has no breakdown comp");
                }
                else
                {
                    Log.Message($"Can break {thing.Label} ? {SandstormCanBreakDown(breakdownComp)}");
                }
            }
        }
    }
}
