﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Sandstorms
{
    public class GameCondition_Sandstorm : GameCondition
    {
        public override WeatherDef ForcedWeather()
        {
            return NCSS_WeatherDefOf.NCSS_Sandstorm;
        }

        public override void End()
        {
            base.End();
            foreach (Map map in AffectedMaps)
            {
                map.weatherManager.TransitionTo(WeatherDefOf.Clear);
            }
        }
    }
}
