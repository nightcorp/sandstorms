﻿using RimWorld;
using RimWorld.Planet;
using System;
using Sandstorms.Settings;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Sandstorms
{
    public class IncidentWorker_Sandstorm : IncidentWorker_MakeGameCondition
    {
        protected override bool CanFireNowSub(IncidentParms parms)
        {
            if(!base.CanFireNowSub(parms))
            {
                return false;
            }

            if (NCSS_Mod.Settings.RequireSandstormBiomeNearby)
            {
                int tile = parms.target.Tile;
                List<int> deserts = WorldUtility.FindNearbyDeserts(tile);
                if (deserts.NullOrEmpty())
                {
                    return false;
                }
            }
            return true;
        }
    }
}
