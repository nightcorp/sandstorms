﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace Sandstorms
{
    [DefOf]
    public static class NCSS_WeatherDefOf
    {
        static NCSS_WeatherDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(NCSS_WeatherDefOf));
        }

        public static WeatherDef NCSS_Sandstorm;
    }
    [DefOf]
    public static class NCSS_EffecterDefOf
    {
        static NCSS_EffecterDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(NCSS_EffecterDefOf));
        }

        public static EffecterDef NCSS_ClearSand;
    }
    [DefOf]
    public static class NCSS_TerrainDefOf
    {
        static NCSS_TerrainDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(NCSS_TerrainDefOf));
        }

        public static TerrainDef SoftSand;
    }
}
