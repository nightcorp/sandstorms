﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Sandstorms
{
    public static class Common
    {
        public static int averageIntervalTicksForBreakdown = Mathf.RoundToInt(NCSS_WeatherDefOf.NCSS_Sandstorm.eventMakers.First(wem => wem.eventClass == typeof(WeatherEvent_ForcedBreakdown)).averageInterval);
    }
}
