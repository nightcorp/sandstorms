﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using RimWorld;

namespace Sandstorms.Settings
{
    public class NCSS_Mod : Mod
    {
        public static NCSS_ModSettings Settings;

        public NCSS_Mod(ModContentPack content) : base(content)
        {
            Settings = GetSettings<NCSS_ModSettings>();
        }

        public override string SettingsCategory() => "NCSS_Settings_SettingsCategory".Translate();
        public override void DoSettingsWindowContents(Rect inRect)
        {
            Settings.FillRect(inRect);
        }
    }

    public class NCSS_ModSettings : ModSettings
    {
        const bool EnableLeftOverSand_Default = true;
        public bool EnableLeftOverSand = EnableLeftOverSand_Default;
        const bool EnableBreakdowns_Default = true;
        public bool EnableBreakdowns = EnableBreakdowns_Default;
        const float ChanceToBreakDown_Default = 0.1f;
        public float ChanceToBreakDown = ChanceToBreakDown_Default;
        const bool UseAlternativeSandTexture_Default = false;
        public bool UseAlternativeSandTexture = UseAlternativeSandTexture_Default;
        const bool RequireSandstormBiomeNearby_Default = true;
        public bool RequireSandstormBiomeNearby = RequireSandstormBiomeNearby_Default;
        const int DistanceToSearchForSandstormBiome_Default = 30;
        const int DistanceToSearchForSandstormBiome_Min = 2;
        const int DistanceToSearchForSandstormBiome_Max = 100;
        public int DistanceToSearchForSandstormBiome = DistanceToSearchForSandstormBiome_Default;
        const bool ClearSandDuringSandstorms_Default = false;
        public bool ClearSandDuringSandstorms = ClearSandDuringSandstorms_Default;
        const bool CanFallenSandBeOnSandTerrain_Default = false;
        public bool CanFallenSandBeOnSandTerrain = CanFallenSandBeOnSandTerrain_Default;
        const float NaturalSandBuildupMultiplier_Default = 1;
        const float NaturalSandBuildupMultiplier_Min = 0;
        const float NaturalSandBuildupMultiplier_Max = 10;
        public float NaturalSandBuildupMultiplier = NaturalSandBuildupMultiplier_Default;
        const float NaturalSandRemovalMultiplier_Default = 1;
        const float NaturalSandRemovalMultiplier_Min = 0;
        const float NaturalSandRemovalMultiplier_Max = 10;
        public float NaturalSandRemovalMultiplier = NaturalSandRemovalMultiplier_Default;

        public void FillRect(Rect inRect)
        {
            Listing_Standard list = new Listing_Standard();
            list.Begin(inRect);

            list.CheckboxLabeled("NCSS_Settings_EnableLeftOverSand".Translate(), ref EnableLeftOverSand, "NCSS_Settings_EnableLeftOverSand_Tip".Translate());
            list.CheckboxLabeled("NCSS_Settings_EnableBreakdowns".Translate(), ref EnableBreakdowns, "NCSS_Settings_EnableBreakdowns_Tip".Translate());
            if (EnableBreakdowns)
            {
                list.Indent();
                list.Label("NCSS_Settings_ChanceToBreakDown".Translate(ChanceToBreakDown.ToStringByStyle(ToStringStyle.PercentTwo)));
                if(ChanceToBreakDown != ChanceToBreakDown_Default)
                {
                    string breakdownIntervalLabel = Common.averageIntervalTicksForBreakdown.ToStringTicksToPeriodVerbose();
                    list.Label("NCSS_Settings_ChanceToBreakDown_Tip".Translate(breakdownIntervalLabel.Named("AVERAGEINTERVAL")));
                }
                ChanceToBreakDown = list.Slider(ChanceToBreakDown, 0.001f, 1f);
                list.Outdent();
            }
            list.CheckboxLabeled("NCSS_Settings_UseAlternativeSandTexture".Translate(), ref UseAlternativeSandTexture, "NCSS_Settings_UseAlternativeSandTexture_Tip".Translate());
            list.CheckboxLabeled("NCSS_Settings_RequireSandstormBiomeNearby".Translate(), ref RequireSandstormBiomeNearby, "NCSS_Settings_RequireSandstormBiomeNearby_Tip".Translate());
            if (RequireSandstormBiomeNearby)
            {
                list.Indent();
                list.Label("NCSS_Settings_DistanceToSearchForSandstormBiome".Translate(DistanceToSearchForSandstormBiome));
                DistanceToSearchForSandstormBiome = (int)list.Slider(DistanceToSearchForSandstormBiome, DistanceToSearchForSandstormBiome_Min, DistanceToSearchForSandstormBiome_Max);
                list.Outdent();
            }

            NamedArgument readableSandBuildupMultiplier = NaturalSandBuildupMultiplier.ToStringByStyle(ToStringStyle.FloatTwo).Named("MULTIPLIER");
            list.Label("NCSS_Settings_NaturalSandBuildupMultiplier".Translate(readableSandBuildupMultiplier), tooltip: "NCSS_Settings_NaturalSandBuildupMultiplier_Tip".Translate());
            NaturalSandBuildupMultiplier = list.Slider(NaturalSandBuildupMultiplier, NaturalSandBuildupMultiplier_Min, NaturalSandBuildupMultiplier_Max);
            NamedArgument readableSandRemovalMultiplier = NaturalSandRemovalMultiplier.ToStringByStyle(ToStringStyle.FloatTwo).Named("MULTIPLIER");
            list.Label("NCSS_Settings_NaturalSandRemovalMultiplier".Translate(readableSandRemovalMultiplier), tooltip: "NCSS_Settings_NaturalSandRemovalMultiplier_Tip".Translate());
            NaturalSandRemovalMultiplier = list.Slider(NaturalSandRemovalMultiplier, NaturalSandRemovalMultiplier_Min, NaturalSandRemovalMultiplier_Max);

            list.CheckboxLabeled("NCSS_Settings_ClearSandDuringSandstorms".Translate(), ref ClearSandDuringSandstorms, "NCSS_Settings_ClearSandDuringSandstorms_Tip".Translate());
            list.CheckboxLabeled("NCSS_Settings_CanFallenSandBeOnSandTerrain".Translate(), ref CanFallenSandBeOnSandTerrain, "NCSS_Settings_CanFallenSandBeOnSandTerrain_Tip".Translate());
            list.Gap();
            if (list.ButtonText("NCSS_Settings_Reset".Translate()))
            {
                Reset();
            }
            list.End();
        }

        public void Reset()
        {
            EnableLeftOverSand = EnableBreakdowns_Default;
            EnableBreakdowns = EnableBreakdowns_Default;
            ChanceToBreakDown = ChanceToBreakDown_Default;
            UseAlternativeSandTexture = UseAlternativeSandTexture_Default;
            RequireSandstormBiomeNearby = RequireSandstormBiomeNearby_Default;
            DistanceToSearchForSandstormBiome = DistanceToSearchForSandstormBiome_Default;
            ClearSandDuringSandstorms = ClearSandDuringSandstorms_Default;
            CanFallenSandBeOnSandTerrain = CanFallenSandBeOnSandTerrain_Default;
            NaturalSandBuildupMultiplier = NaturalSandBuildupMultiplier_Default;
            NaturalSandRemovalMultiplier = NaturalSandRemovalMultiplier_Default;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref EnableLeftOverSand, nameof(EnableLeftOverSand), EnableLeftOverSand_Default);
            Scribe_Values.Look(ref EnableBreakdowns, nameof(EnableBreakdowns), EnableBreakdowns_Default);
            Scribe_Values.Look(ref ChanceToBreakDown, nameof(ChanceToBreakDown), ChanceToBreakDown_Default);
            Scribe_Values.Look(ref UseAlternativeSandTexture, nameof(UseAlternativeSandTexture), UseAlternativeSandTexture_Default);
            Scribe_Values.Look(ref RequireSandstormBiomeNearby, nameof(RequireSandstormBiomeNearby), RequireSandstormBiomeNearby_Default);
            Scribe_Values.Look(ref DistanceToSearchForSandstormBiome, nameof(DistanceToSearchForSandstormBiome), DistanceToSearchForSandstormBiome_Default);
            Scribe_Values.Look(ref ClearSandDuringSandstorms, nameof(ClearSandDuringSandstorms), ClearSandDuringSandstorms_Default);
            Scribe_Values.Look(ref CanFallenSandBeOnSandTerrain, nameof(CanFallenSandBeOnSandTerrain), CanFallenSandBeOnSandTerrain_Default);
            Scribe_Values.Look(ref NaturalSandBuildupMultiplier, nameof(NaturalSandBuildupMultiplier), NaturalSandBuildupMultiplier_Default);
            Scribe_Values.Look(ref NaturalSandRemovalMultiplier, nameof(NaturalSandRemovalMultiplier), NaturalSandRemovalMultiplier_Default);
        }
    }
}
