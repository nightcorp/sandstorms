﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld;
using HarmonyLib;
using System.Reflection;
using UnityEngine;
using System.Runtime.CompilerServices;
using Mono.Security.Authenticode;
using System.Reflection.Emit;

namespace Sandstorms.Patches
{
    /// <summary>
    /// After the snow grid is accessed, access the sand grid and take the max of both grids, then check that to the thingDef.hideAtSnowDepth
    /// Original:
    /// ...
    /// || base.Map.snowGrid.GetDepth(thing.Position) <= thing.def.hideAtSnowDepth
    /// ...
    /// 
    /// Patched: 
    /// || Mathf.Max(base.Map.snowGrid.GetDepth(thing.Position), base.Map.GetComponent<SandGrid>().GetDepthAt(thing.Position)/(float)255) <= thing.def.hideAtSnowDepth
    /// 
    /// </summary>
    [HarmonyPatch(typeof(SectionLayer_Things), "Regenerate")]
    public static class Patch_SectionLayer_Things
    {
        static readonly MethodInfo takePrintFromInfo = AccessTools.Method(typeof(SectionLayer_Things), "TakePrintFrom");
        static readonly FieldInfo snowGridFI = AccessTools.Field(typeof(Map), nameof(Map.snowGrid));
        static readonly MethodInfo hiddenBySnowOrSandInfo = AccessTools.Method(typeof(VisibilityUtility), nameof(VisibilityUtility.SandOrSnowDepth));
        [HarmonyTranspiler]
        public static IEnumerable<CodeInstruction> RemoveIfHiddenBySand(IEnumerable<CodeInstruction> instructions)
        {
            List<CodeInstruction> codeInstructions = instructions.ToList();
            // there is only a single access to SnowGrid
            int anchorIndex = codeInstructions.FindIndex(ci => ci.LoadsField(snowGridFI));
            anchorIndex -= 2;
            codeInstructions.RemoveRange(anchorIndex, 6);
            codeInstructions.InsertRange(anchorIndex, InjectedInstructions());
            return codeInstructions.AsEnumerable();
        }

        private static IEnumerable<CodeInstruction> InjectedInstructions()
        {
            yield return new CodeInstruction(OpCodes.Ldloc_S, 6);
            yield return new CodeInstruction(OpCodes.Call, hiddenBySnowOrSandInfo);
        }

    }
}
