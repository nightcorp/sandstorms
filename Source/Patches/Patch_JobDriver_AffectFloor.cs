﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;

namespace Sandstorms.Patches
{


    [HarmonyPatch(typeof(JobDriver_AffectFloor), "MakeNewToils")]
    public static class Patch_JobDriver_AffectFloor
    {
        [HarmonyPostfix]
        public static IEnumerable<Toil> ClearSand(IEnumerable<Toil> __result, JobDriver_AffectFloor __instance, bool ___clearSnow)
        {
            foreach (Toil toil in __result)
            {
                yield return toil;
            }
            if (___clearSnow)
            {
                Toil clearToil = ToilMaker.MakeToil();
                clearToil.initAction = () =>
                {
                    if (___clearSnow)
                    {
                        NCSS_JobUtility.ClearSandAtTargetA(__instance);
                    }
                };
                yield return clearToil;
            }
        }

    }
}
