﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Sandstorms.Patches
{
    [HarmonyPatch(typeof(StatPart_EnvironmentalEffects))]
    public static class Patch_StatPart_EnvironmentalEffects
    {
        const float deteriorationRateMultiplier = 2.5f;

        [HarmonyPatch("TransformValue")]
        [HarmonyPostfix]
        public static void AddDeteriorationRate(StatRequest req, ref float val)
        {
            Thing thing = req.Thing;
            if (!IsActiveFor(thing))
            {
                return;
            }
            if (thing?.Map?.weatherManager?.CurWeatherLerped != NCSS_WeatherDefOf.NCSS_Sandstorm)
            {
                return;
            }
            val *= deteriorationRateMultiplier;
        }

        [HarmonyPatch("ExplanationPart")]
        [HarmonyPostfix]
        public static void AddExplanation(ref string __result, StatRequest req)
        {
            Thing thing = req.Thing;
            if (!IsActiveFor(thing))
            {
                return;
            }
            if (thing?.Map?.weatherManager?.CurWeatherLerped != NCSS_WeatherDefOf.NCSS_Sandstorm)
            {
                return;
            }
            __result += $"\n{"NCSS_DeterioratingSandstorm".Translate(deteriorationRateMultiplier.ToStringPercent()).CapitalizeFirst()}";
        }

        private static bool IsActiveFor(Thing thing)
        {
            return thing != null && thing.Spawned && thing.def.deteriorateFromEnvironmentalEffects;
        }
    }
}
