﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;
using HarmonyLib;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;
using System.Runtime.CompilerServices;
using Verse.AI;

namespace Sandstorms
{
    [HarmonyPatch(typeof(MouseoverReadout), "MouseoverReadoutOnGUI")]
    public static class Patch_MouseoverReadout
    {
        static Patch_MouseoverReadout()
        {
            FieldInfo botLeftFI = AccessTools.Field(typeof(MouseoverReadout), "BotLeft");
            BotLeft = (Vector2)botLeftFI.GetValue(null);
        }

        private static Vector2 BotLeft;
        private static readonly FieldInfo snowGridFI = AccessTools.Field(typeof(Map), nameof(Map.snowGrid));
        private static readonly MethodInfo displaySandMI = AccessTools.Method(typeof(Patch_MouseoverReadout), nameof(Patch_MouseoverReadout.DisplaySand));
        const float yOffsetPerEntry = 19f;

        [HarmonyTranspiler]
        public static IEnumerable<CodeInstruction> AddSandDetails(IEnumerable<CodeInstruction> instructions)
        {
            List<CodeInstruction> codeInstructions = instructions.ToList();

            int anchorIndex = codeInstructions.FindIndex(ci => ci.LoadsField(snowGridFI));
            anchorIndex += 3;

            codeInstructions.InsertRange(anchorIndex, InjectedInstructions());

            return codeInstructions.AsEnumerable();
        }

        private static IEnumerable<CodeInstruction> InjectedInstructions()
        {
            yield return new CodeInstruction(OpCodes.Ldloca_S, 1);
            yield return new CodeInstruction(OpCodes.Call, displaySandMI);
        }

        private static void DisplaySand(ref float curYOffset)
        {
            IntVec3 cell = UI.MouseCell();
            SnowCategory sandDepthCategory = Find.CurrentMap.GetComponent<SandGrid>().GetDepthAsCategory(cell);

            if (sandDepthCategory == SnowCategory.None)
            {
                return;
            }
            float rectY = UI.screenHeight - BotLeft.y - curYOffset;
            Rect rect = new Rect(BotLeft.x, rectY, 999f, 999f);
            string walkSpeedString = GenPath.SpeedPercentString(SnowUtility.MovementTicksAddOn(sandDepthCategory));
            string label = $"{"NCSS_FallenSand_Label".Translate()} ({SnowUtility.GetDescription(sandDepthCategory)}) ({"WalkSpeed".Translate(walkSpeedString)})";
            Widgets.Label(rect, label);
            curYOffset += yOffsetPerEntry;
        }
    }
}
