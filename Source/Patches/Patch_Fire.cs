﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;
using HarmonyLib;

namespace Sandstorms
{

    [HarmonyPatch(typeof(Fire), "DoComplexCalcs")]
    public static class Patch_Fire
    {
        [HarmonyPostfix]
        public static void ExtinguishFireWithSand(ref Fire __instance)
        {
            if (!__instance.Spawned)
            {
                return;
            }
            if(__instance.Map.weatherManager.CurWeatherLerped != NCSS_WeatherDefOf.NCSS_Sandstorm)
            {
                return;
            }
            if (!SandUtility.SandCanBeOn(__instance.Position, __instance.Map, out _))
            {
                return;
            }

            DamageInfo damage = new DamageInfo(DamageDefOf.Extinguish, 10f);
            __instance.TakeDamage(damage);
        }
    }
}
