﻿using HarmonyLib;
using RimWorld;
using Sandstorms.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace Sandstorms
{
    [HarmonyPatch(typeof(WorkGiver_ClearSnow), nameof(WorkGiver_ClearSnow.HasJobOnCell))]
    public static class Patch_WorkGiver_ClearSnow
    {
        const float sandRequiredToTriggerClearing = 0.2f;
        [HarmonyPostfix]
        public static void TriggerOnSand(ref bool __result, Pawn pawn, IntVec3 c, bool forced)
        {
            if (__result)
            {
                return;
            }
            if (!NCSS_Mod.Settings.ClearSandDuringSandstorms && pawn.Map.weatherManager.CurWeatherLerped == NCSS_WeatherDefOf.NCSS_Sandstorm)
            {
                return;
            }
            if(pawn.Map.GetComponent<SandGrid>().GetDepth(c) < sandRequiredToTriggerClearing)
            {
                return;
            }
            if (c.IsForbidden(pawn))
            {
                return;
            }
            if (!pawn.CanReserve(c, ignoreOtherReservations: forced))
            {
                return;
            }
            __result = true;
        }
    }
}
