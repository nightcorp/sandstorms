﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld;
using HarmonyLib;
using Verse.AI;
using System.Reflection.Emit;
using UnityEngine;
using System.Reflection;

namespace Sandstorms
{
    /// <summary>
    /// Original
    /// int num2 = SnowUtility.MovementTicksAddOn(map.snowGrid.GetCategory(c));
    /// Replacement:
    /// int num2 = SnowUtility.MovementTicksAddOn(map.snowGrid.GetCategory(c));
    /// num = OverwriteWithSandMovementIfPossible(num2, map, c);
    /// </summary>
    [HarmonyPatch(typeof(PathGrid), nameof(PathGrid.CalculatedCostAt))]
    public static class Patch_PathGrid
    {
        private static readonly MethodInfo movementTicksAddOnMI = AccessTools.Method(typeof(SnowUtility), nameof(SnowUtility.MovementTicksAddOn));

        [HarmonyTranspiler]
        public static IEnumerable<CodeInstruction> ConsiderFallenSand(IEnumerable<CodeInstruction> instructions)
        {
            List<CodeInstruction> codeInstructions = instructions.ToList();

            int anchorIndex = codeInstructions.FindIndex(ci => ci.Calls(movementTicksAddOnMI));
            anchorIndex += 2;

            codeInstructions.InsertRange(anchorIndex, InjectedInstructions());

            return codeInstructions.AsEnumerable();
        }

        private static IEnumerable<CodeInstruction> InjectedInstructions()
        {
            yield return new CodeInstruction(OpCodes.Ldloc_S, 4);   // num2 (movementCost)
            yield return new CodeInstruction(OpCodes.Ldarg_0);  // this
            yield return new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(typeof(PathGrid), "map"));    //.map
            yield return new CodeInstruction(OpCodes.Ldarg_1);  // c (gridCell)
            yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(Patch_PathGrid), nameof(OverwriteWithSandMovementIfPossible)));    //Patch_PathGrid.OverwriteWithSandMovementIfPossible
            yield return new CodeInstruction(OpCodes.Stloc_S, 4); // num2 (movementCost)
        }

        private static int OverwriteWithSandMovementIfPossible(int originalMovement, Map map, IntVec3 cell)
        {
            SnowCategory sandDephtCategory = map.GetComponent<SandGrid>().GetDepthAsCategory(cell);
            int sandMovement = SnowUtility.MovementTicksAddOn(sandDephtCategory);
            return Math.Max(originalMovement, sandMovement);
        }
    }
}
