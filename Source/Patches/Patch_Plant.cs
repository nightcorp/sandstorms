﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using Verse;
using RimWorld;

namespace Sandstorms.Patches
{

    [HarmonyPatch(typeof(Plant), nameof(Plant.IngestibleNow), MethodType.Getter)]
    public static class Patch_Plant
    {
        [HarmonyPostfix]
        public static void PreventEatingWhileCoveredInSand(ref bool __result, ref Plant __instance)
        {
            if (!__result)
            {
                return;
            }
            if(__instance == null)
            {
                return;
            }
            __result = VisibilityUtility.CanBeSeenUnderSand(__instance);
        }
    }
}
