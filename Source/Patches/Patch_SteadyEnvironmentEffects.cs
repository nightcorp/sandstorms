﻿using HarmonyLib;
using RimWorld;
using Sandstorms.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.Noise;

namespace Sandstorms
{
    /// <summary>
    /// Patching on the hook that short circuiting rain uses
    /// </summary>
    [HarmonyPatch(typeof(SteadyEnvironmentEffects))]
    public static class Patch_SteadyEnvironmentEffects
    {
        #region deterioration rate
        [HarmonyPatch("FinalDeteriorationRate", new Type[] { typeof(Thing), typeof(bool), typeof(bool), typeof(TerrainDef), typeof(List<string>) })]
        [HarmonyPostfix]
        public static void AddSandstormDeterioration(Thing t, bool roomUsesOutdoorTemperature, List<string> reasons)
        {
            if (!roomUsesOutdoorTemperature)
            {
                return;
            }
            if (t.SpawnedOrAnyParentSpawned && !t.def.canDeteriorateUnspawned)
            {
                return;
            }
            if (t?.Map?.weatherManager?.CurWeatherLerped != NCSS_WeatherDefOf.NCSS_Sandstorm)
            {
                return;
            }
            if(reasons == null)
            {
                reasons = new List<string>();
            }
            reasons.Add("NCSS_DeterioratingSandstorm".Translate());
        }
        #endregion

        #region fallen sand
        static Dictionary<Map, SandGrid> cachedSandGrids = new Dictionary<Map, SandGrid>();
        static float cachedSandAddRate;
        static float cachedSandRemovalRate;
        const float baseSandRemoveRate = 0.02f;
        const float minSandAdd = 0.09f;
        const float maxSandAdd = 0.10f;
        [HarmonyPatch("DoCellSteadyEffects")]
        [HarmonyPostfix]
        public static void DoSandEffects(IntVec3 c, Map ___map)
        {
            if(!cachedSandGrids.ContainsKey(___map))
            {
                cachedSandGrids[___map] = ___map.GetComponent<SandGrid>();
            }
            if (!NCSS_Mod.Settings.EnableLeftOverSand)
            {
                return;
            }
            bool canAddSand = SandUtility.SandCanBeOn(c, ___map, out bool shouldRemoveSandImmediately);

            if (shouldRemoveSandImmediately)
            {
                cachedSandGrids[___map].RemoveSand(c);
            }
            if (!canAddSand)
            {
                return;
            }
            RecacheSandValues(___map);
            canAddSand &= ___map.weatherManager.CurWeatherLerped == NCSS_WeatherDefOf.NCSS_Sandstorm;
            if (canAddSand)
            {
                SandAddingEffects(c, ___map);
            }
            else
            {
                SandRemovalEffects(c, ___map);
            }
        }

        private static void SandAddingEffects(IntVec3 cell, Map map)
        {
            float sandValue = Rand.Range(minSandAdd, maxSandAdd);

            sandValue *= cachedSandAddRate;
            cachedSandGrids[map].AddDepth(cell, sandValue);
        }

        private static void SandRemovalEffects(IntVec3 cell, Map map)
        {
            if(NCSS_Mod.Settings.NaturalSandRemovalMultiplier <= 0)
            {
                return;
            }
            if (!cachedSandGrids[map].AnySandAvailable)
            {
                return;
            }

            cachedSandGrids[map].AddDepth(cell, -cachedSandRemovalRate);
        }

        static int lastCachedTick = -1;
        const int ticksBeforeMapRecache = GenTicks.TickRareInterval;
        public static void RecacheSandValues(Map map)
        {
            if(lastCachedTick == -1 || lastCachedTick + ticksBeforeMapRecache > GenTicks.TicksGame)
            {
                cachedSandAddRate = SandUtility.CurrentSandRate(map);
                cachedSandAddRate *= NCSS_Mod.Settings.NaturalSandBuildupMultiplier;
                cachedSandRemovalRate = map.windManager.WindSpeed * baseSandRemoveRate * (1 - cachedSandAddRate);
                cachedSandRemovalRate *= NCSS_Mod.Settings.NaturalSandRemovalMultiplier;
                lastCachedTick = GenTicks.TicksGame;
            }
        }
        #endregion
    }
}
