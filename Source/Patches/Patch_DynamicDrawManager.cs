﻿#if v1_4
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld;
using HarmonyLib;
using System.Reflection;
using UnityEngine;
using System.Runtime.CompilerServices;
using Mono.Security.Authenticode;
using System.Reflection.Emit;

namespace Sandstorms.Patches
{
    /// <summary>
    /// Basically exactly the same code changes as <see cref="Patch_SectionLayer_Things"/>
    /// </summary>
    [HarmonyPatch(typeof(DynamicDrawManager), "DrawDynamicThings")]
    public static class Patch_DynamicDrawManager
    {
        static readonly MethodInfo takePrintFromInfo = AccessTools.Method(typeof(SectionLayer_Things), "TakePrintFrom");
        static readonly FieldInfo snowGridFI = AccessTools.Field(typeof(Map), nameof(Map.snowGrid));
        static readonly MethodInfo hiddenBySnowOrSandInfo = AccessTools.Method(typeof(VisibilityUtility), nameof(VisibilityUtility.SandOrSnowDepth));
        [HarmonyTranspiler]
        public static IEnumerable<CodeInstruction> RemoveIfHiddenBySand(IEnumerable<CodeInstruction> instructions)
        {
            List<CodeInstruction> codeInstructions = instructions.ToList();
            // there is only a single access to SnowGrid
            int anchorIndex = codeInstructions.FindIndex(ci => ci.LoadsField(snowGridFI));
            anchorIndex -= 2;
            codeInstructions.RemoveRange(anchorIndex, 5);
            codeInstructions.InsertRange(anchorIndex, InjectedInstructions());
            return codeInstructions.AsEnumerable();
        }

        private static IEnumerable<CodeInstruction> InjectedInstructions()
        {
            yield return new CodeInstruction(OpCodes.Ldloc_S, 5);
            yield return new CodeInstruction(OpCodes.Call, hiddenBySnowOrSandInfo);
        }

    }
}
#endif