﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;
using HarmonyLib;
using System.Reflection;

namespace Sandstorms
{
    [HarmonyPatch(typeof(CellInspectorDrawer), "DrawMapInspector")]
    public static class Patch_CellInspectorDrawer
    {
        [HarmonyPostfix]
        public static void AddSandCategory()
        {
            IntVec3 cell = UI.MouseCell();
            Map map = Find.CurrentMap;
            SnowCategory sandDepthAtCell = map.GetComponent<SandGrid>().GetDepthAsCategory(cell);
            if(sandDepthAtCell == SnowCategory.None)
            {
                return;
            }
            Reroute_DrawRow("NCSS_FallenSand_Label".Translate(), SnowUtility.GetDescription(sandDepthAtCell).CapitalizeFirst());
        }

        static Action<string, string> drawRowDelegate;
        /// <summary>
        /// Private static method. Reflection will call the original <see cref="CellInspectorDrawer.DrawRow"/>
        /// </summary>
        private static void Reroute_DrawRow(string label, string info)
        {
            if(drawRowDelegate == default)
            {
                MethodInfo drawRowMI = AccessTools.Method(typeof(CellInspectorDrawer), "DrawRow");
                drawRowDelegate = Delegate.CreateDelegate(typeof(Action<string, string>), drawRowMI) as Action<string, string>;
            }

            drawRowDelegate(label, info);
        }
    }
}
