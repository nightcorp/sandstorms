﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;

namespace Sandstorms.Patches
{


    [HarmonyPatch(typeof(JobDriver_ConstructFinishFrame), "MakeNewToils")]
    public static class Patch_JobDriver_ConstructFinishFrame
    {
        [HarmonyPostfix]
        public static IEnumerable<Toil> ClearSand(IEnumerable<Toil> __result, JobDriver_ConstructFinishFrame __instance)
        {
            foreach (Toil toil in __result)
            {
                yield return toil;
            }

            BuildableDef entityDefToBuild = __instance.job?.targetA.Thing?.def?.entityDefToBuild;
            if (entityDefToBuild != null && entityDefToBuild is TerrainDef)
            {
                Toil clearToil = ToilMaker.MakeToil();
                clearToil.initAction = () =>
                {
                    NCSS_JobUtility.ClearSandAtTargetA(__instance);
                };
                yield return clearToil;
            };
        }
    }
}
